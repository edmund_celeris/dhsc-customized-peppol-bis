<?xml version="1.0" encoding="UTF-8"?>
<pattern id="NHS-CheckResponseModel" xmlns="http://purl.oclc.org/dsdl/schematron">
	
	<rule context="//ubl:OrderResponse" flag="fatal" role="error">
		<assert test="cbc:CustomizationID = 'urn:www.cenbii.eu:transaction:biitrns076:ver2.0:extended:urn:www.peppol.eu:bis:peppol28a:ver1.0:extended:urn:peppol-authority.co.uk:spec:ordering:ver2.1'" 
		flag="fatal" id="DHR-2">[DH-T01-R010]	An Order must use the DH Ordering Customization ID - DHOrderRespDDIndex2</assert>
		<assert test="cbc:OrderResponseCode = '11' or cbc:OrderResponseCode = '12' or cbc:OrderResponseCode ='27' or cbc:OrderResponseCode='29' or cbc:OrderResponseCode='30'"
		flag="fatal" id="DHR-7">[DH-T76-R001]	An Order Response must have an OrderTypeCode -  DHOrderRespDDIndex7</assert>
	</rule>
</pattern>



