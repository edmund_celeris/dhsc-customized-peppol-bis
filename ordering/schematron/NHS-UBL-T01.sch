<?xml version="1.0" encoding="UTF-8"?>  
<pattern id="NHS-CheckorderingModel" xmlns="http://purl.oclc.org/dsdl/schematron">
	
	<rule context="//ubl:Order" flag="warning">
		<assert test="cbc:CustomizationID = 'urn:www.cenbii.eu:transaction:biitrns001:ver2.0:extended:urn:www.peppol.eu:bis:peppol28a:ver1.0:extended:urn:fdc:peppol-authority.co.uk:spec:ordering:ver1.0'" 
		flag="fatal" role="error" id="DH-2">[DH-T01-R010]	An Order must use the DH Ordering Customization ID - DHOrderDDIndex2</assert>
		<assert test="cbc:OrderTypeCode = '105' or cbc:OrderTypeCode = '220' or cbc:OrderTypeCode ='227' or cbc:OrderTypeCode='402'" flag="fatal" id="DH-7">[DH-T01-R001]	An Order should have a valid Order Type code -  DHOrderDDIndex7
		</assert>
	</rule>
	
	<!-- BuyerCustomerParty check -->  
	<rule context="cac:BuyerCustomerParty" flag="fatal" role="error" id="BuyerCustomerPartyIDCheck"> 
        
		<assert test="cac:Party/cbc:EndpointID !=''" id="DH-16">[DH-T01-R002] An Order must have a Buyers electronic address (EndPoint) -DHOrderDDIndex16</assert>
		<assert test="cac:Party/cac:PartyIdentification/cbc:ID !=''" id="DH-17">[DH-T01-R005] An Order must have the Buyers Party Identification ID. -DHOrderDDIndex17</assert>
		
	</rule>  
	
	<rule context="cac:BuyerCustomerParty/cac:Party" flag="warning" role="warning" id="BuyerCustomerPartyAddressCheck"> 
		<assert test="cac:PartyIdentification/cbc:ID/@schemeID ='GLN'" id="DH-17a">Note: The Buyer Party Identification ID SchemeID is not GLN -DHOrderDDIndex17</assert>
		<assert test="cac:PartyName/cbc:Name !=''" id="DH-18">The BuyerCustomerParty Name is missing -DHOrderDDIndex18</assert>
		<assert test="exists(cac:PostalAddress)" id="DH-19-23">BuyerCustomerParty address is missing </assert>
	</rule>
	<rule context="cac:BuyerCustomerParty/cac:Party/cac:PostalAddress" flag="warning" id="BuyerCustomerPartyAddressDetailCheck"> 
		<assert test="exists(cbc:AdditionalStreetName)" id="DH-19">BuyerCustomerParty AdditionalStreetName (Building name) is not in the postal Address segment -DHOrderDDIndex19</assert>
		<assert test="exists(cbc:StreetName)" id="DH-20">BuyerCustomerParty StreetName is not in the postal Address segment -DHOrderDDIndex20</assert>
		<assert test="cbc:CityName !=''" id="DH-21">BuyerCustomerParty City/Town is missing -DHOrderDDIndex21</assert>
		<assert test="exists(cbc:PostalZone)" id="DH-23">BuyerCustomerParty PostCode is not in the postal Address segment -DHOrderDDIndex23</assert>
		
	</rule>    
	
	<!-- SellerSupplierParty check -->  
	<rule context="cac:SellerSupplierParty" flag="fatal" role="error" id="SellerSupplierPartyPartyIDCheck"> 
		
		<assert test="cac:Party/cbc:EndpointID !=''" id="DH-27">[DH-T01-R003] An Order must have a Sellers electronic address (EndPoint) -DHOrderDDIndex27</assert>
		<assert test="cac:Party/cac:PartyIdentification/cbc:ID !=''" id="DH-28">[DH-T01-R006] An Order must have the Sellers Party Identification ID. -DHOrderDDIndex28</assert>
		
	</rule>  
	<rule context="cac:SellerSupplierParty/cac:Party" flag="warning" role="warning" id="SellerSupplierPartyAddressCheck"> 
		<assert test="cac:PartyIdentification/cbc:ID/@schemeID ='GLN'" id="DH-28a">Note: The Supplier PartyID SchemeID is not a GLN -DHOrderDDIndex28</assert>
		<assert test="cac:PartyName/cbc:Name !=''" id="DH-29">The Seller Supplier Party Name is missing</assert>
		<assert test="exists(cac:PostalAddress)" id="dh30-34">You must provide an address for the SellerSupplierParty</assert>
	</rule>
	<rule context="cac:SellerSupplierParty/cac:Party/cac:PostalAddress" flag="warning" id="SellerSupplierPartyAddressDetailCheck">
		<assert test="exists(cbc:AdditionalStreetName)" id="DH-30">BuyerCustomerParty AdditionalStreetName (Building) is not in the postal Address segment -DHOrderDDIndex19</assert>
		<assert test="exists(cbc:StreetName)" id="DH-31">BuyerCustomerParty StreetName is not in the postal Address segment -DHOrderDDIndex31</assert>
		<assert test="cbc:CityName !=''" id="DH-32">BuyerCustomerParty City/Town missing -DHOrderDDIndex32</assert>
		<assert test="exists(cbc:PostalZone)" id="DH-34">BuyerCustomerParty PostCode not in the postal Address segment -DHOrderDDIndex34</assert>
		
	</rule>    
	
	<!-- AccountingCustomerParty check -->  
	<rule context="cac:AccountingCustomerParty" flag="fatal" id="AccountingCustomerPartyIDCheck"> 
		
		
		<assert test="cac:Party/cac:PartyIdentification/cbc:ID !=''" id="DH-36">DH-T01-R007	An Order must have the Accounting Party Identification ID. DHOrderDDIndex36</assert>
		
	</rule>  
	<rule context="cac:AccountingCustomerParty/cac:Party" flag="warning" id="AccountingCustomerPartyAddressCheck"> 
		<assert test="cbc:EndpointID !=''" id="DH-35">DH-T01-R004	An Order should have a Accounting Party electronic address (EndPoint)-DHOrderDDIndex35</assert>
		<assert test="cac:PartyIdentification/cbc:ID/@schemeID ='GLN'" id="DH-36a">The AccountingCustomerParty Identification ID SchemeID is not a GLN -DHOrderDDIndex36</assert>
		<assert test="cac:PartyName/cbc:Name !=''" id="DH-38">[DHIndex22] AccountingCustomerParty Party Name missing -DHOrderDDIndex36</assert>
		<assert test="exists(cac:PostalAddress)" id="DH39-43">You must provide an address for the BuyerCustomerParty</assert>
	</rule>
	<rule context="cac:AccountingCustomerParty/cac:Party/cac:PostalAddress" flag="warning" id="AccountingCustomerPartyAddressDetailCheck"> 
		<assert test="exists(cbc:AdditionalStreetName)" id="DH-39">AccountingCustomerParty AdditionalStreetName (Building) not in the postal Address segment -DHOrderDDIndex39</assert>
		<assert test="exists(cbc:StreetName)" id="DH-40">AccountingCustomerParty StreetName not in the postal Address segment -DHOrderDDIndex40</assert>
		<assert test="cbc:CityName !=''" id="DH-41">AccountingCustomerParty City/Town missing -DHOrderDDIndex41</assert>
		<assert test="exists(cbc:PostalZone)" id="DH-43">AccountingCustomerParty PostCode not in the postal Address segment -DHOrderDDIndex43</assert>
		
	</rule> 
	
	
	<!-- Deliverycheck -->  
	<rule context="cac:Delivery/cac:DeliveryLocation/cac:Address" flag="fatal"  role="error" id="deliveryIDcheck"> 
		<assert test="cbc:ID !=''" id="DH-44">[DH-T01-R008] An Order must have the Delivery Location Address ID -DHOrderDDIndex44</assert>
	</rule>
	
	<rule context="cac:Delivery/cac:DeliveryLocation" flag="warning" role="warning" id="deliveryAddresscheck"> 
		<assert test="exists(cac:Address/cbc:AdditionalStreetName)" id="DH-45">DeliveryLocation Address AdditionalStreetName not in the postal Address segment -DHOrderDDIndex45</assert>
		<assert test="exists(cac:Address/cbc:StreetName)" id="DH-46">DeliveryLocation StreetName not in the postal Address segment -DHOrderDDIndex46</assert>
		<assert test="cac:Address/cbc:CityName !=''" id="DH-47">DeliveryLocation City not in the postal Address segment -DHOrderDDIndex47</assert>
		<assert test="exists(cac:Address/cbc:CountrySubentity)" id="DH-48">DeliveryLocation County not in the postal Address segment -DHOrderDDIndex48</assert>
		<assert test="exists(cac:Address/cbc:PostalZone)" id="DH-49">DeliveryLocation PostCode not in the postal Address segment</assert>
		
	</rule>    
	
	<rule context="cac:OrderLine/cac:LineItem" flag="warning" role="warning" id="linecheck">
		
		<assert test="cac:Item/cac:StandardItemIdentification/cbc:ID !=''" id="DH-96">[DH-T01-R009] An Order Line Item should have a Standard Identification ID.  </assert>
		
	</rule>
	
	<rule context="cac:OrderLine/cac:LineItem/cac:Item" flag="fatal" role="error" id="linecheckItemID">
		<assert test="cbc:Name !=''" id="DH-94">[DHIndex94] invoice Line must have the Item Name</assert>
		<assert test="exists(cac:BuyersItemIdentification/cbc:ID) or  exists(cac:StandardItemIdentification/cbc:ID) or exists(cac:SellersItemIdentification/cbc:ID)" id="DH-86a">an invoice Line must have at least one of the following; BuyersItemIdentification StandardItemIdentification or SellersItemIdentification -DHOrderDDIndex86</assert>
		
	</rule>
</pattern>

